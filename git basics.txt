Microsoft Windows [Version 10.0.19042.572]
(c) 2020 Microsoft Corporation. All rights reserved.

C:\Users\saranya.bashyam>cd Desktop

C:\Users\saranya.bashyam\Desktop>cd git_demo

C:\Users\saranya.bashyam\Desktop\git_demo>git init .
Initialized empty Git repository in C:/Users/saranya.bashyam/Desktop/git_demo/.git/

C:\Users\saranya.bashyam\Desktop\git_demo>git status
On branch master

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        file1.txt

nothing added to commit but untracked files present (use "git add" to track)

C:\Users\saranya.bashyam\Desktop\git_demo>git add .

C:\Users\saranya.bashyam\Desktop\git_demo>git status
On branch master

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)
        new file:   file1.txt


C:\Users\saranya.bashyam\Desktop\git_demo>git commit -m "Initial commit"
[master (root-commit) 4526f08] Initial commit
 Committer: Saranya Bashyam <saranya.bashyam@ycs.in>
Your name and email address were configured automatically based
on your username and hostname. Please check that they are accurate.
You can suppress this message by setting them explicitly. Run the
following command and follow the instructions in your editor to edit
your configuration file:

    git config --global --edit

After doing this, you may fix the identity used for this commit with:

    git commit --amend --reset-author

 1 file changed, 1 insertion(+)
 create mode 100644 file1.txt

C:\Users\saranya.bashyam\Desktop\git_demo>git status
On branch master
nothing to commit, working tree clean

C:\Users\saranya.bashyam\Desktop\git_demo>git status
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   file1.txt

no changes added to commit (use "git add" and/or "git commit -a")

C:\Users\saranya.bashyam\Desktop\git_demo>git add .

C:\Users\saranya.bashyam\Desktop\git_demo>git commit -m "Added further code
[master 89c7fcd] Added further code
 Committer: Saranya Bashyam <saranya.bashyam@ycs.in>
Your name and email address were configured automatically based
on your username and hostname. Please check that they are accurate.
You can suppress this message by setting them explicitly. Run the
following command and follow the instructions in your editor to edit
your configuration file:

    git config --global --edit

After doing this, you may fix the identity used for this commit with:

    git commit --amend --reset-author

 1 file changed, 1 insertion(+), 1 deletion(-)

C:\Users\saranya.bashyam\Desktop\git_demo>git status
On branch master
nothing to commit, working tree clean

C:\Users\saranya.bashyam\Desktop\git_demo>git branc -a
git: 'branc' is not a git command. See 'git --help'.

C:\Users\saranya.bashyam\Desktop\git_demo>git branch a

C:\Users\saranya.bashyam\Desktop\git_demo>git branch -a
  a
* master

C:\Users\saranya.bashyam\Desktop\git_demo>
C:\Users\saranya.bashyam\Desktop\git_demo>
C:\Users\saranya.bashyam\Desktop\git_demo>
C:\Users\saranya.bashyam\Desktop\git_demo>
C:\Users\saranya.bashyam\Desktop\git_demo>
C:\Users\saranya.bashyam\Desktop\git_demo>git branch 0-a

C:\Users\saranya.bashyam\Desktop\git_demo>git branch -a
  0-a
  a
* master

C:\Users\saranya.bashyam\Desktop\git_demo>git branch -d 0-a
Deleted branch 0-a (was 89c7fcd).

C:\Users\saranya.bashyam\Desktop\git_demo>git branch -d a
Deleted branch a (was 89c7fcd).

C:\Users\saranya.bashyam\Desktop\git_demo>git branch -a
* master

C:\Users\saranya.bashyam\Desktop\git_demo>git branch dev_1

C:\Users\saranya.bashyam\Desktop\git_demo>git branch -a
  dev_1
* master

C:\Users\saranya.bashyam\Desktop\git_demo>git checkout dev_1
Switched to branch 'dev_1'

C:\Users\saranya.bashyam\Desktop\git_demo>git checkout -b dev_2
Switched to a new branch 'dev_2'

C:\Users\saranya.bashyam\Desktop\git_demo>git status
On branch dev_2
nothing to commit, working tree clean

C:\Users\saranya.bashyam\Desktop\git_demo>git status
On branch dev_2
nothing to commit, working tree clean

C:\Users\saranya.bashyam\Desktop\git_demo>git branch -a
  dev_1
* dev_2
  master

C:\Users\saranya.bashyam\Desktop\git_demo>git status
On branch dev_2
Untracked files:
  (use "git add <file>..." to include in what will be committed)
        git_commands.txt

nothing added to commit but untracked files present (use "git add" to track)

C:\Users\saranya.bashyam\Desktop\git_demo>git add .

C:\Users\saranya.bashyam\Desktop\git_demo>git commit -m "created git_commands file"
[dev_2 84959c6] created git_commands file
 Committer: Saranya Bashyam <saranya.bashyam@ycs.in>
Your name and email address were configured automatically based
on your username and hostname. Please check that they are accurate.
You can suppress this message by setting them explicitly. Run the
following command and follow the instructions in your editor to edit
your configuration file:

    git config --global --edit

After doing this, you may fix the identity used for this commit with:

    git commit --amend --reset-author

 1 file changed, 33 insertions(+)
 create mode 100644 git_commands.txt

C:\Users\saranya.bashyam\Desktop\git_demo>git checkout master
Switched to branch 'master'

C:\Users\saranya.bashyam\Desktop\git_demo>git checkout dev_2
Switched to branch 'dev_2'

C:\Users\saranya.bashyam\Desktop\git_demo>git checkout master
Switched to branch 'master'

C:\Users\saranya.bashyam\Desktop\git_demo>git merge dev_2
Updating 89c7fcd..84959c6
Fast-forward
 git_commands.txt | 33 +++++++++++++++++++++++++++++++++
 1 file changed, 33 insertions(+)
 create mode 100644 git_commands.txt

C:\Users\saranya.bashyam\Desktop\git_demo>cd ..

C:\Users\saranya.bashyam\Desktop>mkdir tenjin

C:\Users\saranya.bashyam\Desktop>cd tenjin

C:\Users\saranya.bashyam\Desktop\tenjin>git clone https://gitlab.com/yethi-rnd/web-automation/web-automation-backend
Cloning into 'web-automation-backend'...
fatal: unable to access 'https://gitlab.com/yethi-rnd/web-automation/web-automation-backend.git/': OpenSSL SSL_read: Connection was reset, errno 10054

C:\Users\saranya.bashyam\Desktop\tenjin>git clone https://gitlab.com/yethi-rnd/web-automation/web-automation-backend
Cloning into 'web-automation-backend'...
fatal: unable to access 'https://gitlab.com/yethi-rnd/web-automation/web-automation-backend/': Could not resolve host: gitlab.com

C:\Users\saranya.bashyam\Desktop\tenjin>git clone https://gitlab.com/yethi-rnd/web-automation/web-automation-backend
Cloning into 'web-automation-backend'...
warning: redirecting to https://gitlab.com/yethi-rnd/web-automation/web-automation-backend.git/
remote: Enumerating objects: 90, done.
remote: Counting objects: 100% (90/90), done.
remote: Compressing objects: 100% (68/68), done.
Receiving objects:  16% (15/90)used 0 (delta 0), pack-reused 0R
Receiving objects: 100% (90/90), 61.73 KiB | 6.17 MiB/s, done.
Resolving deltas: 100% (18/18), done.

C:\Users\saranya.bashyam\Desktop\tenjin>dir
 Volume in drive C has no label.
 Volume Serial Number is 563B-8F2D

 Directory of C:\Users\saranya.bashyam\Desktop\tenjin

24-11-2020  18:54    <DIR>          .
24-11-2020  18:54    <DIR>          ..
24-11-2020  18:54    <DIR>          web-automation-backend
               0 File(s)              0 bytes
               3 Dir(s)  179,096,702,976 bytes free

C:\Users\saranya.bashyam\Desktop\tenjin>cd web-automation-backend

C:\Users\saranya.bashyam\Desktop\tenjin\web-automation-backend>git status
On branch master
Your branch is up to date with 'origin/master'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   src/main/resources/application.properties

no changes added to commit (use "git add" and/or "git commit -a")

C:\Users\saranya.bashyam\Desktop\tenjin\web-automation-backend>git add .

C:\Users\saranya.bashyam\Desktop\tenjin\web-automation-backend>git commit -m "Dummy change in application.properties"
[master c799eaa] Dummy change in application.properties
 Committer: Saranya Bashyam <saranya.bashyam@ycs.in>
Your name and email address were configured automatically based
on your username and hostname. Please check that they are accurate.
You can suppress this message by setting them explicitly. Run the
following command and follow the instructions in your editor to edit
your configuration file:

    git config --global --edit

After doing this, you may fix the identity used for this commit with:

    git commit --amend --reset-author

 1 file changed, 2 insertions(+)

C:\Users\saranya.bashyam\Desktop\tenjin\web-automation-backend>git status
On branch master
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)

nothing to commit, working tree clean

C:\Users\saranya.bashyam\Desktop\tenjin\web-automation-backend>git push
warning: redirecting to https://gitlab.com/yethi-rnd/web-automation/web-automation-backend.git/
Enumerating objects: 11, done.
Counting objects: 100% (11/11), done.
Delta compression using up to 8 threads
Compressing objects: 100% (6/6), done.
Writing objects: 100% (6/6), 539 bytes | 269.00 KiB/s, done.
Total 6 (delta 2), reused 0 (delta 0), pack-reused 0
remote: GitLab: You are not allowed to push code to protected branches on this project.
To https://gitlab.com/yethi-rnd/web-automation/web-automation-backend
 ! [remote rejected] master -> master (pre-receive hook declined)
error: failed to push some refs to 'https://gitlab.com/yethi-rnd/web-automation/web-automation-backend'

C:\Users\saranya.bashyam\Desktop\tenjin\web-automation-backend>git checkout -b saranya_changes
Switched to a new branch 'saranya_changes'

C:\Users\saranya.bashyam\Desktop\tenjin\web-automation-backend>git status
On branch saranya_changes
nothing to commit, working tree clean

C:\Users\saranya.bashyam\Desktop\tenjin\web-automation-backend>dir
 Volume in drive C has no label.
 Volume Serial Number is 563B-8F2D

 Directory of C:\Users\saranya.bashyam\Desktop\tenjin\web-automation-backend

24-11-2020  18:54    <DIR>          .
24-11-2020  18:54    <DIR>          ..
24-11-2020  18:54               428 .gitignore
24-11-2020  18:54    <DIR>          .mvn
24-11-2020  18:54            10,380 mvnw
24-11-2020  18:54             6,790 mvnw.cmd
24-11-2020  18:54             2,319 pom.xml
24-11-2020  18:54    <DIR>          src
               4 File(s)         19,917 bytes
               4 Dir(s)  179,094,704,128 bytes free

C:\Users\saranya.bashyam\Desktop\tenjin\web-automation-backend>git status
On branch saranya_changes
nothing to commit, working tree clean

C:\Users\saranya.bashyam\Desktop\tenjin\web-automation-backend>git push
fatal: The current branch saranya_changes has no upstream branch.
To push the current branch and set the remote as upstream, use

    git push --set-upstream origin saranya_changes


C:\Users\saranya.bashyam\Desktop\tenjin\web-automation-backend>git push -u origin saranya_changes
warning: redirecting to https://gitlab.com/yethi-rnd/web-automation/web-automation-backend.git/
Enumerating objects: 11, done.
Counting objects: 100% (11/11), done.
Delta compression using up to 8 threads
Compressing objects: 100% (6/6), done.
Writing objects: 100% (6/6), 539 bytes | 269.00 KiB/s, done.
Total 6 (delta 2), reused 0 (delta 0), pack-reused 0
remote:
remote: To create a merge request for saranya_changes, visit:
remote:   https://gitlab.com/yethi-rnd/web-automation/web-automation-backend/-/merge_requests/new?merge_request%5Bsource_branch%5D=saranya_changes
remote:
To https://gitlab.com/yethi-rnd/web-automation/web-automation-backend
 * [new branch]      saranya_changes -> saranya_changes
Branch 'saranya_changes' set up to track remote branch 'saranya_changes' from 'origin'.

C:\Users\saranya.bashyam\Desktop\tenjin\web-automation-backend>git status
On branch saranya_changes
Your branch is up to date with 'origin/saranya_changes'.

nothing to commit, working tree clean

C:\Users\saranya.bashyam\Desktop\tenjin\web-automation-backend>git checkout master
Switched to branch 'master'
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)

C:\Users\saranya.bashyam\Desktop\tenjin\web-automation-backend>git pull
warning: redirecting to https://gitlab.com/yethi-rnd/web-automation/web-automation-backend.git/
remote: Enumerating objects: 1, done.
remote: Counting objects: 100% (1/1), done.
remote: Total 1 (delta 0), reused 0 (delta 0), pack-reused 0
Unpacking objects: 100% (1/1), 286 bytes | 31.00 KiB/s, done.
From https://gitlab.com/yethi-rnd/web-automation/web-automation-backend
   1eb4076..c2b673b  master     -> origin/master
Updating c799eaa..c2b673b
Fast-forward

C:\Users\saranya.bashyam\Desktop\tenjin\web-automation-backend>git branch -d saranya_changes
Deleted branch saranya_changes (was c799eaa).

C:\Users\saranya.bashyam\Desktop\tenjin\web-automation-backend>